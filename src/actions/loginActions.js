const setUser = (userObj) => {
    return {
        type: "SET_USER",
        payload: userObj
    }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    setUser
}