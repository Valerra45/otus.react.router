import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import NavNemu from './components/Layout';
import Home from './components/HomePage';
import Login from './components/Login';
import Register from './components/Register';
import NotFound from './components/NotFound';

import './App.css';

function App() {
  return (
    <div className="App">
      <NavNemu>
      <Router>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/register">
            <Register />
          </Route>
          <Route>
            <NotFound/>
          </Route>
        </Switch>
        </Router>
      </NavNemu>
    </div>
  );
}

export default App;
