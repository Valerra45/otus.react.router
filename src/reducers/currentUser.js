const defaultState = {
    user: null
};

const currentUser = (state = defaultState, action) => {
    switch (action.type) {
        case "SET_USER":
            console.log(action.payload)
            return {
                ...state,
                user: action.payload,
            }
            default:
                return state;
    }
}

export default currentUser;