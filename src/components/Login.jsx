import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux'
import allActions from '../actions'
import Button from 'react-bootstrap/Button'

const Login = () => {
    const [login, setUserName] = useState('');

    const dispatch = useDispatch();

    const currentUser = useSelector((state) => state.currentUser.user);

    const onLogin = () => {
         dispatch(allActions.loginActions.setUser(login));
      };

    return (
        <div className="Login">
            {currentUser != null &&
                <h2>Hi! {currentUser}</h2>
            }
            <div className="mb-3">
                <label>Username: </label>
                <input type="text" placeholder="Username" onChange={e => setUserName(e.target.value)} required />
            </div>
            <div className="mb-3">
                <label>Password: </label>
                <input type="password" placeholder="Password" required />
            </div>
            <Button  variant="primary" type="submit" className="button" onClick = {() => onLogin()}>
                Login
            </Button >
        </div>
    );
};

export default Login;