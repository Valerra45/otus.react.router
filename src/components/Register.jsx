import Button from 'react-bootstrap/Button'

const Register = () => {
    return (
        <div className="Login">
            <div className="mb-3">
                <label>Username: </label>
                <input type="text" placeholder="Username"required />
            </div>
            <div className="mb-3">
                <label>Password: </label>
                <input type="password" placeholder="Password" required />
            </div>
            <div className="mb-3">
                <label>Confirm: </label>
                <input type="password" placeholder="Confirm password" required />
            </div>
            <Button  variant="primary" type="submit" className="button">
                Register
            </Button >
        </div>
    );
};

export default Register;