import { Navbar, Nav, Container } from 'react-bootstrap'

export default function NavMenu() {
    return (
        <Navbar bg="light" variant="light">
            <Container>
                <Navbar.Brand href="/">React-Bootstrap</Navbar.Brand>
                <Nav className="me-auto">
                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href="/login">Login</Nav.Link>
                    <Nav.Link href="/register">Register</Nav.Link>
                </Nav>
            </Container>
        </Navbar>
    );
}